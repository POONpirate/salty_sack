local size = GetModConfigData("salty_sack_size")
local perishpriority = GetModConfigData("perishpriority")

Assets = {
    Asset("ATLAS", "images/inventoryimages/icepack_token.xml"),
    Asset("IMAGE", "images/inventoryimages/icepack_token.tex"),
    Asset("ATLAS", "images/inventoryimages/krampus_sack_token.xml"),
    Asset("IMAGE", "images/inventoryimages/krampus_sack_token.tex"),
    Asset("ATLAS", "images/inventoryimages/salty_sack.xml"),
    Asset("IMAGE", "images/inventoryimages/salty_sack.tex"),
    Asset("ATLAS", "images/krampus_sack_bg.xml"),
    Asset("IMAGE", "images/krampus_sack_bg.tex"),
}

PrefabFiles =
{
	"icepack_token",
	"krampus_sack_token",
    "salty_sack",
}

local icepack_token_ing = GLOBAL.Ingredient("icepack_token",1)
icepack_token_ing.atlas = "images/inventoryimages/icepack_token.xml"
icepack_token_ing.image = "icepack_token.tex"

local krampus_sack_token_ing = GLOBAL.Ingredient("krampus_sack_token",1)
krampus_sack_token_ing.atlas = "images/inventoryimages/krampus_sack_token.xml"
krampus_sack_token_ing.image = "krampus_sack_token.tex"

local icepack_token = AddRecipe("icepack_token", {GLOBAL.Ingredient("bearger_fur", 1), GLOBAL.Ingredient("gears", 1), GLOBAL.Ingredient("transistor", 1)}, GLOBAL.RECIPETABS.SURVIVAL, GLOBAL.TECH.SCIENCE_TWO)
icepack_token.atlas = "images/inventoryimages/icepack_token.xml"
icepack_token.image = "icepack_token.tex"
local icepack_token_sortkey = GLOBAL.AllRecipes["icepack"]["sortkey"]
icepack_token.sortkey = icepack_token_sortkey + 0.1
GLOBAL.STRINGS.NAMES.ICEPACK_TOKEN = "Insulated pack token"
GLOBAL.STRINGS.RECIPE_DESC.ICEPACK_TOKEN = "Could be useful."
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.ICEPACK_TOKEN = "I wonder what this is for."

local krampus_sack = AddRecipe("krampus_sack", {krampus_sack_token_ing}, GLOBAL.RECIPETABS.SURVIVAL, GLOBAL.TECH.SCIENCE_TWO)
local krampus_sack_sortkey = GLOBAL.AllRecipes["icepack"]["sortkey"]
krampus_sack.sortkey = krampus_sack_sortkey + 0.2
GLOBAL.STRINGS.NAMES.KRAMPUS_SACK_TOKEN = "Krampus sack token"
GLOBAL.STRINGS.RECIPE_DESC.KRAMPUS_SACK = "Could be useful."
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.KRAMPUS_SACK_TOKEN = "I wonder what this is for."

local salty_sack = AddRecipe("salty_sack", {icepack_token_ing,krampus_sack_token_ing,GLOBAL.Ingredient("saltrock",30)}, GLOBAL.RECIPETABS.SURVIVAL, GLOBAL.TECH.SCIENCE_TWO)
salty_sack.atlas = "images/inventoryimages/salty_sack.xml"
salty_sack.image = "salty_sack.tex"
local salty_sack_sortkey = GLOBAL.AllRecipes["icepack"]["sortkey"]
salty_sack.sortkey = salty_sack_sortkey + 0.3
GLOBAL.STRINGS.NAMES.SALTY_SACK = "Salty sack"
GLOBAL.STRINGS.RECIPE_DESC.SALTY_SACK = "I don't want my food anywhere near that."
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.SALTY_SACK = "It's a pretty gross thing to put on my back."


local containers = require "containers"

local params = {}

params.salty_sack8 =
{
	widget =
	{
	slotpos = {},
	animbank = "ui_backpack_2x4",
	animbuild = "ui_backpack_2x4",
	pos = GLOBAL.Vector3(-5, -70, 0),
	},
	issidewidget = true,
	type = "pack",
}

for y = 0, 3 do
	table.insert(params.salty_sack8.widget.slotpos, GLOBAL.Vector3(-162, -75 * y + 114, 0))
	table.insert(params.salty_sack8.widget.slotpos, GLOBAL.Vector3(-162 + 75, -75 * y + 114, 0))
end

params.salty_sack10 =
{
	widget =
	{
	slotpos = {},
	animbank = "ui_krampusbag_2x5",
	animbuild = "ui_krampusbag_2x5",
	pos = GLOBAL.Vector3(-5, -70, 0)
	},
	issidewidget = true,
	type = "pack",
}

for y = 0, 4 do
		table.insert(params.salty_sack10.widget.slotpos, GLOBAL.Vector3(-162, -75 * y + 115, 0))
		table.insert(params.salty_sack10.widget.slotpos, GLOBAL.Vector3(-162 + 75, -75 * y + 115, 0))
end

params.salty_sack12 =
{
	widget =
	{
	slotpos = {},
	animbank = "ui_piggyback_2x6",
	animbuild = "ui_piggyback_2x6",
	pos = GLOBAL.Vector3(-5, -50, 0)
	},
	issidewidget = true,
	type = "pack",
}

for y = 0, 5 do
		table.insert(params.salty_sack12.widget.slotpos, GLOBAL.Vector3(-162, -75 * y + 170, 0))
		table.insert(params.salty_sack12.widget.slotpos, GLOBAL.Vector3(-162 + 75, -75 * y + 170, 0))
end

params.salty_sack14 =
{
	widget =
	{
	slotpos = {},
	animbank = "ui_krampusbag_2x8",
	animbuild = "ui_krampusbag_2x8",
	pos = GLOBAL.Vector3(-5, -120, 0)
	},
	issidewidget = true,
	type = "pack",
}

for y = 0, 6 do
		table.insert(params.salty_sack14.widget.slotpos, GLOBAL.Vector3(-162, -y*75 + 240 ,0))
		table.insert(params.salty_sack14.widget.slotpos, GLOBAL.Vector3(-162 +75, -y*75 + 240 ,0))
end

params.salty_sack16 =
{
	widget =
	{
	slotpos = {},
	animbank = "ui_krampusbag_2x8",
	animbuild = "ui_krampusbag_2x8",
	pos = GLOBAL.Vector3(-5, -50, 0)
	},
	issidewidget = true,
	type = "pack",
}

for y = 0, 7 do
	table.insert(params.salty_sack16.widget.slotpos, GLOBAL.Vector3(-162, -65 * y + 245, 0))
	table.insert(params.salty_sack16.widget.slotpos, GLOBAL.Vector3(-162 + 75, -65 * y + 245, 0))
end

params.salty_sack18 =
{
	widget =
	{
	slotpos = {},
	animbank = nil,
	animbuild = nil,
	bgatlas = "images/krampus_sack_bg.xml",
	bgimage = "krampus_sack_bg.tex",
	pos = GLOBAL.Vector3(-76,-70,0)
	},
	issidewidget = true,
	type = "pack",
}

for y = 0, 8 do
	table.insert(params.salty_sack18.widget.slotpos, GLOBAL.Vector3(-37, -y*75 + 300 ,0))
	table.insert(params.salty_sack18.widget.slotpos, GLOBAL.Vector3(-37 +75, -y*75 + 300 ,0))
end


local function priorityfn(container, item, slot)
	return item.components.perishable
end

if perishpriority then
	params.salty_sack8.priorityfn = priorityfn
	params.salty_sack10.priorityfn = priorityfn
	params.salty_sack12.priorityfn = priorityfn
	params.salty_sack14.priorityfn = priorityfn
	params.salty_sack16.priorityfn = priorityfn
	params.salty_sack18.priorityfn = priorityfn
end

for k, v in pairs(params) do
	containers.MAXITEMSLOTS = math.max(containers.MAXITEMSLOTS, v.widget.slotpos ~= nil and #v.widget.slotpos or 0)
end


local containers_widgetsetup = containers.widgetsetup

function containers.widgetsetup(container, prefab, data)
	local t = data or params[prefab or container.inst.prefab]
	if t ~= nil then
		for k, v in pairs(t) do
			container[k] = v
		end
		container:SetNumSlots(container.widget.slotpos ~= nil and #container.widget.slotpos or 0)    
	else
		return containers_widgetsetup(container, prefab, data)
	end
end