local assets =
{
    Asset("ANIM", "anim/backpacktokens.zip"),
    Asset("ATLAS", "images/inventoryimages/icepack_token.xml"),    
    Asset("IMAGE", "images/inventoryimages/icepack_token.tex"),
}

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)
    
    inst.AnimState:SetBank("backpacktokens")
    inst.AnimState:SetBuild("backpacktokens")
    inst.AnimState:PlayAnimation("icepack_token")

    MakeInventoryFloatable(inst, "med", nil, 0.77)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")
    inst:AddComponent("inventoryitem")
	inst.components.inventoryitem.imagename = "icepack_token"
	inst.components.inventoryitem.atlasname = "images/inventoryimages/icepack_token.xml"

    MakeHauntableLaunch(inst)

    return inst
end

return Prefab("icepack_token", fn, assets)