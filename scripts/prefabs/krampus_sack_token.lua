local assets =
{
    Asset("ANIM", "anim/backpacktokens.zip"),
    Asset("ATLAS", "images/inventoryimages/krampus_sack_token.xml"),    
    Asset("IMAGE", "images/inventoryimages/krampus_sack_token.tex"),
}

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)
    
    inst.AnimState:SetBank("backpacktokens")
    inst.AnimState:SetBuild("backpacktokens")
    inst.AnimState:PlayAnimation("krampus_sack_token")

    MakeInventoryFloatable(inst, "med", nil, 0.77)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")
    inst:AddComponent("inventoryitem")
	inst.components.inventoryitem.imagename = "krampus_sack_token"
	inst.components.inventoryitem.atlasname = "images/inventoryimages/krampus_sack_token.xml"

    MakeHauntableLaunch(inst)

    return inst
end

return Prefab("krampus_sack_token", fn, assets)