local slotConfig = GetModConfigData("salty_sack_size", KnownModIndex:GetModActualName("Salty sack"))
local coolthermal = GetModConfigData("salty_sack_cool_thermal", KnownModIndex:GetModActualName("Salty sack"))
local perishmult = GetModConfigData("salty_sack_perish_mult", KnownModIndex:GetModActualName("Salty sack"))
local keepondrown = GetModConfigData("keepondrown", KnownModIndex:GetModActualName("Salty sack"))

local assets =
{
	Asset("ANIM", "anim/salty_sack.zip"),
	Asset("ANIM", "anim/swap_salty_sack.zip"),
}

local function onequip(inst, owner)
    owner.AnimState:OverrideSymbol("backpack", "swap_salty_sack", "backpack")
    owner.AnimState:OverrideSymbol("swap_body", "swap_salty_sack", "swap_body")
    if inst.components.container ~= nil then
        inst.components.container:Open(owner)
    end
end

local function onunequip(inst, owner)
    owner.AnimState:ClearOverrideSymbol("swap_body")
    owner.AnimState:ClearOverrideSymbol("backpack")
    if inst.components.container ~= nil then
        inst.components.container:Close(owner)
    end
    inst.AnimState:PlayAnimation("idle")
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("salty_sack")
    inst.AnimState:SetBuild("salty_sack")
    inst.AnimState:PlayAnimation("idle")

    inst:AddTag("backpack")

    inst.foleysound = "dontstarve/movement/foley/backpack"

    MakeInventoryFloatable(inst, "small", 0.2)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        inst.OnEntityReplicated = function(inst) inst.replica.container:WidgetSetup(slotConfig) end
        return inst
    end

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.cangoincontainer = false
	if keepondrown then
		inst.components.inventoryitem.keepondrown = true
	end
    inst.components.inventoryitem.atlasname = "images/inventoryimages/salty_sack.xml"
    inst.components.inventoryitem.imagename = "salty_sack"

    inst:AddComponent("equippable")
	if not EQUIPSLOTS.BACK then
		inst.components.equippable.equipslot = EQUIPSLOTS.BODY
	else
		inst.components.equippable.equipslot = EQUIPSLOTS.BACK
	end
    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)

    inst:AddComponent("container")
    inst.components.container:WidgetSetup(slotConfig)
    inst.components.container.skipclosesnd = true
    inst.components.container.skipopensnd = true
	
	if perishmult then
		inst:AddComponent("preserver")
		if perishmult == "saltbox" then
			inst.components.preserver:SetPerishRateMultiplier(TUNING.PERISH_SALTBOX_MULT)
		elseif perishmult == "never" then
			inst.components.preserver:SetPerishRateMultiplier(0)
		else
			inst.components.preserver:SetPerishRateMultiplier(TUNING.PERISH_FRIDGE_MULT)
		end
	end
	
	if not coolthermal then
		inst:AddTag("nocool")
	end

    MakeHauntableLaunchAndDropFirstItem(inst)

    return inst
end

return Prefab("salty_sack", fn, assets, prefabs)
