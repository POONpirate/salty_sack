name = "Salty sack"
description = [[
A portmanteau of the salt box and Krampus sack, the salty sack is the perfect solution to keep your food fresh on the go!

Recipe: (At Alchemy Engine in the Science Tab)
-- 1 Insulated bag token
-- 1 Krampus sack token
-- 30 salt crystals

Insulated bag token costs the same ingredients as the bag.
Krampus sack token is from deconstructing a Krampus sack.

Configurations:
-- Size: Choose how many slots the bag will have.
-- Cool thermal: Choose if thermal stones will cool in the bag. Also makes ice stay frozen.
-- Perish multiplier: Choose how well the bag will preserve food.
]]

author = "Kelso"
version = "1.10"
forumthread = ""
api_version = 10
dst_compatible = true

all_clients_require_mod = true
client_only_mod = false
server_filter_tags = {}

icon_atlas = "images/modicon.xml"
icon = "modicon.tex"

----------------------------
-- Configuration settings --
----------------------------


configuration_options = 
{
  {
		name = "salty_sack_size",
		label = "Salty sack size",
		hover = "The number of item slots in the salty sack.",
		options =	
		{
		    {description = "8 slots", data = "salty_sack8"},
		    {description = "10 slots", data =  "salty_sack10"},
		    {description = "12 slots", data =  "salty_sack12"},
		    {description = "14 slots", data =  "salty_sack14"},
            {description = "16 slots", data =  "salty_sack16"},
			{description = "18 slots", data = "salty_sack18"},
		},
		default = "krampus_sack",
	},
  {
		name = "salty_sack_perish_mult",
		label = "Salty sack perish multiplier",
		hover = "How well does the salty sack preserve food?",
		options =	
		{
		    {description = "Salt box preserve", data = "saltbox"},
		    {description = "Ice box preserve", data =  "icebox"},
			{description = "Never perish", data =  "never"},
			{description = "No cooling", data =  false},
		},
		default = "saltbox",
	},
  {
		name = "salty_sack_cool_thermal",
		label = "Salty sack cool thermal stone",
		hover = "Should the salty sack cool thermal stones and keep ice fresh?",
		options =	
		{
		    {description = "Cool thermal", data = true},
		    {description = "Don't cool thermal", data =  false},
		},
		default = false,
	},
  {
		name = "perishpriority",
		label = "Prioritize perishables",
		hover = "Items that are perishable will be put into the sack before your inventory",
		options =	
		{
		    {description = "Prioritize", data = true},
		    {description = "Don't prioritize", data =  false},
		},
		default = true,
	},
  {
		name = "keepondrown",
		label = "Keep on drown",
		hover = "Keep the bag on drowning",
		options =	
		{
		    {description = "Keep", data = true},
		    {description = "Don't keep", data =  false},
		},
		default = true,
	},
}

